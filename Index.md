# Kit d'appropriation Atelier Juridique Science Ouverte (AJSO) 

![cover-kit](./pictures/apercu_KIT_AJSO.jpg)


| :unlock: Un kit accessible en licence Creative Commons (CC BY-SA 4.0) |
| ------------------------------------- |
| Ce kit d'appropriation AJSO est mis à votre disposition pour faciliter l'organisation de tels ateliers au sein de votre institution. Vous trouverez l'ensemble des ressources que nous avons constituées et améliorées au fur et à mesure des ateliers. Soyez libre de les reprendre ou de vous en inspirer. A propos de la [licence CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) |

| :pencil: Comment contribuer ? |
| ------------------------------------- |
| Le contenu de ce kit ainsi que ses annexes sont disponible sur [cet espace GitLab](https://gitlab.com/inno3/AJSO-kit/) <br> N’hésitez à remixer et contribuer en apportant des conseils et retours d’expériences. <br> Pour contacter l'équipe organisatrice de cette série d’atelier, vous pouvez également vous adresser à : atelier-juridique-so@inno3.fr |


**Table des matières**

[TOC]


# Préambule

De novembre 2020 à mai 2021, trois Ateliers Juridiques Science Ouverte (AJSO) ont été organisés. Intégralement en ligne, ils ont réunis en tout plus de 120 personnes aux profils variés (juristes chercheur·e·s, doctorant·e·s, responsables IST, etc.) qui ont échangé et travaillé collectivement sur des questions relatives aux enjeux juridiques de la science ouverte.

Avant de vous plonger dans ce kit, nous vous donnons quelques éléments de contexte sur l'origine des ateliers et la démarche qui les accompagne (co-production de savoirs, posture non surplombante, etc.) se reflétant dans le déroulé même des ateliers.


## Mission « réussir l'appropriation de la science ouverte »
Les AJSO ont été réalisés en 2020/2021 dans le cadre de la [mission « réussir l'appropriation de la science ouverte »](https://www.ouvrirlascience.fr/reussir-lappropriation-de-la-science-ouverte/). Commanditée par l’Université de Paris pour le Comité pour la science ouverte (CoSO) et le collège « données de la recherche », elle s’inscrit dans le cadre des activités du Comité pour la science ouverte, et a bénéficié d’un financement du MESRI. La mission avait pour vocation de connaître et favoriser les pratiques de science ouverte au sein des communautés disciplinaires. 

Complémentaire d’une étude spécifique sur l'usage de données en fonction de communautés de pratiques en recherche, trois ateliers juridiques science ouvertes (AJSO) ont été conçus et réalisés autour de thématiques concernant les données de la recherche :
- AJSO #1 « Au fil des images de la recherche »
- AJSO #2 « Cycle de vie des données : cartographie des enjeux juridiques »
- AJSO #3 « Données personnelles et ouverture »

Les ateliers ont été coordonnés par Juliette Hueber (InVisu/CNRS, InHA) et Lionel Maurel (InSHS, chargé du GT « juridique » du Comité pour la science ouverte) avec l'accompagnement de la société Inno³.




## Impulsion initiale des AJSO et objectifs fixés

La création des AJSO fait suite à plusieurs constats :
1. Les pratiques de science ouverte s'accompagnent d'un __ensemble de transformations juridiques__ qu'il s'agit, pour les spécialistes juridiques, d'appréhender et de mettre en application. C'est le rôle par exemple du groupe de travail juridique du Comité pour la science ouverte.
2. Mettre en application ces évolutions juridiques et nouvelles mesures nécessite des __changements de pratiques__ de la part des communautés de recherche et des services de soutien à la recherche (valorisation, IST, juridique, etc.). 
Plus que des pratiques, il s'agit d'appréhender une __nouvelle culture__, où l'__ouverture devient le principe par défaut__. Ceci s'avère souvent périlleux (manque de connaissance à ce sujet, peu d'accompagnement, frictions entre différents services, injonctions contradictoires, etc.).
3. Les guides juridiques sont essentiels mais ne comblent cependant pas le fossé entre compréhension et appropriation. La __diversité des situations de terrain__ en fonction des disciplines, des démarches de recherche, des parties prenantes d'un projet rendent souvent complexe l’__appropriation des « bonnes pratiques »__ proposées qui demeurent très théoriques. 
Partir des cas concrets et faire se rencontrer différents profils (juridiques et non-juridiques) lors d'ateliers joue un rôle majeur de médiation et permet de mettre en lumière des problématiques de terrain rencontrées ainsi que de faciliter la co-construction de réponses adaptées à la diversité de situations de recherche.


Les objectifs des AJSO ont été ainsi définis dans ce sens :
- Réunir différents profils pour aborder collectivement les questions juridiques qui se posent au sein d'un projet de recherche.
- Trouver collectivement des réponses adaptées et facilement compréhensibles par un public de non-spécialistes des questions juridiques.
- Constituer un réseau d'entraide pour accompagner au plus long terme cette dynamique.


## Design et organisation des ateliers

En amont d’un atelier, la première étape consiste à réfléchir au design (format, communication, facilitation, coordination) de ces ateliers afin d'aider à un échange fécond entre différentes parties prenantes le jour J et à faciliter la constitution de compte-rendu suite à la séance.

Une préparation minutieuse des ateliers (en amont, pendant et après) a permis de trouver un format pour faciliter des dynamiques collaboratives et mettre en avant les capacités d'intelligence collective des participant·e·s tout autant qu'une ambiance conviviale malgré la distance.

| :signal_strength: Une adaptation au contexte sanitaire |
| ------------------------------------- |
| Les conditions sanitaires de 2020-2021 (Covid-19) ont ajouté une difficulté supplémentaire. Initialement prévus en présentiel, les ateliers ont été adaptés pour se dérouler intégralement en ligne. Les AJSO « en ligne » qui, au départ consistait en une « solution de repli », sont devenus le format privilégié avec l'avantage de toucher un plus grand nombre de personnes réparties sur tout le territoire. |


**Les trois ateliers organisés ont permis d'en affiner leur design.**

Le présent kit résulte du travail itératif mené par l’organisation des trois ateliers suivants (vue d’ensemble avec l’intitulé de chacun des 5 groupes de travail) :

| Atelier|
| ------------------------------------- |
| #1 « Au fil des images en recherche » (vendredi 6 novembre 2020) <br> Sujets traités en groupe :  1. Production d'images, connaissances essentielles ; 2. Réutilisation d'images dans l'ESR ; 3. GLAM et données culturelles ; 4. Éditions : activités et contrats ; 5. Vie privée, données personnelles et éthique |
| #2 « Cycle de vie des données : cartographie des enjeux juridiques » (vendredi 16 avril 2021) <br> Sujets traités en groupe : 1. Mise en œuvre du projet ; 2. Collecte/production de données  ; 3. Traitement de données ; 4. Stockage de données ; 5. Publication et dissémination |
| #3 « Données personnelles et ouverture » (vendredi 28 mai 2021) <br> Sujets traités en groupes : 1. Anonymisation ; 2. Consentement ; 3. Exceptions légales et réglementaires ; 4. Données publiquement accessibles ; 5. Partage sécurisé |


## Bilan des premiers AJSO

Une quarantaine de personnes par ateliers se sont retrouvées pour des matinées collectives. Session plénière et session en groupe se sont succédé pour rythmer les 3h30 d'échanges. Suite à une présentation générale des AJSO et une introduction à quelques concepts clefs juridiques sur la thématique traitée, chaque participant·e rejoignait un groupe thématique.

Une trame de facilitation aidait les personnes facilitatrices désignées en amont à guider les échanges et à constituer une synthèse. La session plénière finale offrait la possibilité à chaque groupe de restituer le contenu des travaux menés. La facilitation a été un élément clef pour tirer le meilleur des échanges en groupe et faire ressortir collectivement des questions clefs juridiques provenant du terrain. 

Plus généralement, des pistes d'actions ont été mises en lumière en faisant remonter des besoins des communautés sur des cas précis (modèle de contrat pour les images, besoin d'éclaircissement sur le rôle des licences CC BY-NC, etc.). Les moments en groupes ont aussi été l'occasion de faire se rencontrer des personnes partageant les mêmes problématiques et d'aider à la constitution d'un réseau dynamique de personnes intéressées à agir collectivement, pour trouver des solutions juridiques adaptées aux environnements de recherche dans lesquels elles travaillent.




# La science ouverte : une posture 

Plus qu'un simple partage de ressources, la science ouverte s'est incarnée ici par un format souhaitant casser les barrières entre différents types de savoirs. Partager des connaissances juridiques était tout autant valable qu'apporter des retours d'expérience de terrain. La facilitation avait pour vocation d'éviter toute posture surplombante, de faciliter la traduction entre des vocabulaires et connaissances liées à des compétences-métier ou des disciplines spécifiques. 

## Un atelier 100% open source
Un deuxième aspect éthique fort concernait la volonté d'insérer les ateliers dans un environnement 100% open source. Le parti pris étant d'une part d'intégrer la science ouverte dans des infrastructures éthiques et durables, et d'autre part de rendre la méthodologie appropriable et réplicable. 
Pour cela, dans le cadre des AJSO, le choix a été d'utiliser des outils open source pour la préparation, l'animation et le rendu des ateliers. 

## Liste des outils utilisés
- Mailtrain : liste de diffusion des ateliers
- Framaforms : formulaire d'inscription
- NextCloud : stockage collectif des documents
- LibreOffice : suite bureautique
- BigBlueButton : solution de visioconférence
- Hedgedoc : documents-ressource et prises de notes partagées
- Kanboard : gestion de projets


| :rainbow: Une démarche inclusive |
| ------------------------------------- |
| Une attention à l'inclusion et à une justice épistémique est un élément fondateur de la démarche des AJSO. Cette démarche s'ancre dans une vision de la science ouverte comme accompagnement des processus ouverts et collectifs de recherche et d'enquête pour faciliter la co-construction de connaissances avec les communautés concernées. |




# Organisation du kit


Le kit se décompose en trois parties :
1. Préparatif avant l'atelier 
2. Facilitation pendant l'atelier
3. Synthèse après l'atelier 


En annexe vous trouverez l'ensemble des documents modèles nécessaires à l'organisation d'un atelier et à adapter en fonction de vos besoins. Ces documents se basent sur l'exemple du dernier AJSO "données personnelles et ouverture", réalisé le 28 mai 2021.  

Même si les ateliers ont été réalisés en ligne et que de nombreux éléments concernent la facilitation d'un format à distance, il est tout à fait possible d'adapter ces ateliers à un format en présentiel. 

Bonne navigation ! 


## Politique de contribution et licences 

Les ressources produites à l'issue des ateliers juridiques s'appuient sur des synthèses réalisées à partir des échanges auxquels ils ont donné lieu et ne constituent pas un guide juridique validé par le Comité pour la science ouverte.

Nous vous partageons dans ce kit l'ensemble des contenus produits pour que vous puissiez vous les approprier, les réutiliser et les améliorer.

Si les ateliers ont été développés dans une logique itérative, ils restent néanmoins de nombreuses autres solutions possibles et ce kit gagnera a être enrichi par vos propres expériences.

Mis à disposition  sous  licence  CC BY-SA 4.0, ce kit peut être librement réutilisé, modifié ou non, y compris dans un contexte commercial, à condition de le redistribuer selon les mêmes termes.
