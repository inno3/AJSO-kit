---
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---


# Annexe AC3


## A Propos
- Outil suggéré : HedgeDoc
- Public cible : participant.e.s (public)
- Objectif : Exemple de document récapitulant toutes les informations utiles pour les participants (présentation, déroulement, lien de connexion, équipe, accès à la documentation relative à chaque thématique


Accès rapide :

[TOC]


## Contenu du document


## Titre du document

[AJSO#3] Données personnelles et ouverture - INFORMATIONS UTILES


Bonjour, 

**Vous êtes inscrit.e à l'atelier juridique science ouverte « Données personnelles et ouverture » du vendredi 28 mai de 9h à 12h30.**

Ce document vous servira de référence pour avoir accès à l'ensemble des informations nécessaires et connaître les prochaines étapes.

*Pour toutes questions, veuillez contacter l'adresse : atelier-juridique-so@inno3.fr*


## En amont de l'atelier 

### Présentation et inscription  

Retrouvez [la présentation générale](https://www.ouvrirlascience.fr/atelier-juridique-science-ouverte-donnees-personnelles-et-ouverture/) de l'atelier #3 « Données personnelles et ouverture » ainsi que la [FAQ](./AA2_2021_FAQ_kit-AJSO.md) détaillant les objectifs des ateliers. 

> :bulb: En cas de désistement, veillez à prévenir l'équipe (atelier-juridique-so@inno3.fr) **au moins 24 heures en avance**. Plusieurs personnes sont en liste d'attente. De même, afin de ne pas déséquilibrer les groupes et par respect pour les personnes présentes sur liste d’attente, veuillez ne pas diffuser le lien de connexion.


### Préparation de l'atelier : questions et ressources, répartition en groupes

Afin de préparer au mieux l'atelier, nous vous demandons de lire le document [Préparation de l'atelier (ressources, questions, groupes)](./AG3_2021_Preparation-atelier_kit-AJSO.md) qui indique :
- un certain nombre de ressources partagées
- les questions et retours d'expérience recueillis lors de l'inscription
- la répartition des participant.e.s dans les groupes thématiques pour le format atelier


### Visio-conférence : vos informations de connexion et session test le jeudi 27 mai 2021 de 14h30 à 15h.

Le lien de connexion vous a été communiqué par mail et est aussi indiqué dans le [tutoriel de connexion et de pratiques collaboratives](./2021_Tutoriel-connexion_kit-AJSO.md)

Afin de faciliter un déroulé fluide de l'atelier et d'anticiper tout problème technique et de connexion nous vous proposons de réaliser un test de l'outil BigBlueButton (BBB) le **jeudi 27 mai à 14h30**. 

> :bulb: Si vous n’avez jamais utilisé BBB nous vous recommandons vivement d’y participer pour être certain.e que vos microphones fonctionnent correctement le jour de l'atelier.

> :warning: **Nous ne pourrons pas assurer les réglages de chacun.e pendant l’atelier et cela pourra rendre plus difficile votre participation.**



## Pendant l'atelier 

### Rappel du déroulé 

:clock9: 9h-9h15 Accueil et introduction générale `Plénière`
- Accueil des participant.e.s 
- Rappel logistique et déroulé général 
- Présentation générale des AJSO 
- Objectifs de l'atelier 

:clock930: 9h15-9h45 Contexte juridique et présentation des ateliers `Plénière`
- Présentation du thème et du programme
- Rappel de quelques notions clefs juridiques 
- Présentation des groupes et des facilitat.eurs.rices
- Consignes pour la répartition en sous-groupes et méthodologie de travail

:clock10: 10h-11h30 Ateliers thématiques `Groupe`
- Travail en équipe avec une répartition des différents profils (juristes, chercheur.e.s, personnels IST, etc.) dans chaque groupe
- Accompagnement par les personnes « facilitatrices » de l'atelier

:clock1130: 11h30-11h45 Pause `Plénière`

:clock12: 11h45-12h30 Synthèses et conclusion `Plénière`
- Synthèse des avancées de chaque groupe 
- Conclusion et suite des échanges


### Lien de la salle plénière  

Le lien et le code d'accès à la salle plénière vous ont été communiqué dans le mail de connexion intitulé "*Connexion à l'atelier juridique science ouverte « Données personnelles et ouverture »*". 

> :bulb: **Vous n'avez jamais utilisé BigBlueButton ?** Consultez le [tutoriel](./2021_Tutoriel-connexion_kit-AJSO.md) et participez à la session test le jeudi 27 mai à 14h30.


### Équipe organisatrice et aide apportée lors de l'atelier 

- Juliette Hueber (InViSU INHA/CNRS) `facilitation` , `organisation`
- Lionel Maurel (InSHS) `facilitation juridique`, `organisation`
- Jonathan Keller (Telecom Paris) `facilitation juridique`
- Clémence Bolla (INSERM) `facilitation juridique`
- Timothée Bonnet (Réseau SupDPO) `facilitation juridique`
- Benjamin Jean (Inno³) `facilitation juridique`
- Célya Gruson-Daniel (Inno³) `organisation`
- Maya Anderson-Gonzalez (Inno³) `organisation`



### Prises de notes collaboratives 

Nous proposons pour chaque équipe un document partagé sous la forme d'un [HedgeDoc](https://hedgedoc.org/).

> :bulb: Pour vous familiariser avec les principes de HedgeDoc et de l'écriture en markdown, cliquez sur le bouton aide dans la barre en haut à gauche.

Liste des documents de prise de note :
- Notes session plénière (_lien à ajouter_)
- Groupe 1 : Anonymisation (_lien à ajouter_)
- Groupe 2 : Consentement (_lien à ajouter_)
- Groupe 3 : Exceptions légales et réglementaires (_lien à ajouter_)
- Groupe 4 : Données personelles publiquement accessibles (_lien à ajouter_)
- Groupe 5 : Partage sécurisé (_lien à ajouter_)



## Suite de l'atelier 

### Plateforme Sharelex 

Les questions, cas de figures et ressources proposées à la fin de l'atelier pourront être ajoutés sur la plateforme [Sharelex](https://sharelex.org/c/science-ouverte/65).

N'hésitez pas à parcourir la plateforme. 

