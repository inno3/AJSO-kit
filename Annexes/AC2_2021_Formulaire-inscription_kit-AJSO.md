---
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---

# Annexe AC2


## A Propos
- Outil suggéré : Formulaire
- Public cible : internautes (public)
- Objectif : exemples de formulaires et enquêtes auprès des participant.e.s


Accès rapide :

[TOC]


## Contenu du document

###  Titre du document
Formulaires d'inscription et de satisfaction AJSO sur Framaform 

> Des modèles sont mises à disposition pour être directement clonés sur [framaform](https://framaforms.org/templates?title_1=science+ouverte&name=inno3) 


### Exemple du formulaire d'inscription (AJSO #3)


#### --- Inscription ---

- Nom *
- Prénom *
- Adresse mail *
- Quelle est votre structure de rattachement ? *
- Quelle est votre fonction principale au sein de cette structure ? Précisez *
- Quelle est votre discipline principale ? * 
- Avez-vous déjà participé à un ou des précédents ateliers AJSO ? * 
    - Oui, AJSO#1 "au fil des images de la recherche"
    - Oui, AJSO#2 "cycle de vie des données"
    - Non


#### --- Préparation de l'atelier ---

- Afin de préparer la répartition en groupes, veuillez indiquer vos préférences (2 choix). *
    - 1/ Anonymisation
    - 2/ Consentement
    - 3/ Exceptions légales et réglementaires
    - 4/ Données personnelles publiquement accessibles (réseaux sociaux numériques)
    - 5/ Partage sécurisé

> A noter que nous ne pouvons pas garantir votre premier choix (par souci de constitution de groupes équilibrés)

- Avez-vous déjà des questions ou des expériences juridiques sur ces étapes ? Si oui, lesquelles ?

*Ces dernières seront partagées en amont de l'atelier.*

- Avez-vous des ressources (juridique, cas d'études, article) utiles à partager ?

- Souhaitez-vous participer à une session test de l'outil de visio-conférence Big Blue Button ? Oui/Non

*La session test est prévue le jeudi 27 mai de 14h30 à 15h.* 

- Précisez-nous en commentaire si vous n'êtes pas disponible à cet horaire mais souhaitez participer.

- Avez-vous des commentaires supplémentaires ?


#### --- Suite à l'atelier ---

- Seriez-vous intéressé.e de participer à quelques rencontres supplémentaires sur cette thématique dans les mois qui suivent ? 1 ou 2 rencontres supplémentaires *
    - Oui
    - Non

- Êtes-vous d'accord de contribuer à l'enrichissement du Labolex où les questions et réponses discutées lors de l'atelier seront ensuite inscrites ? *
    - Oui
    - Non


#### --- Envoi d'informations ---

- En soumettant ce formulaire j'accepte que les informations saisies soient exploitées afin de m'envoyer les informations de connexion au webinaire ainsi que les documents utiles à la préparation de l'atelier et aux étapes complémentaires. *
    - Oui

- Sans votre consentement nous ne pourrons vous envoyer vos détails de connexion et votre inscription ne sera pas validée
Cochez cette case si vous souhaitez être être ajouté.e.s à la liste de diffusion des ateliers juridiques science ouverte 
    - Oui




### Exemple de l'enquête de satisfaction (AJSO #3)


#### --- Communication et contenu de l'atelier ---


- Comment avez-vous entendu parler de l'atelier juridique science ouverte « Données personnelles et ouverture » ? *
    - Inno³
    - Université de Paris
    - Ouvrir la science
    - Autre 
- Merci de préciser : *
    - Twitter
    - Linkedin
    - Site internet
    - Liste de diffusion/newsletter

- Si « Autre», merci de préciser : *

- Le contenu de l'atelier *

Echelle employée : Tout à fait en désaccord	/ Plutôt en désaccord / Plutôt d'accord / Tout à fait d'accord / Sans opinion 

Questions : 
- La présentation de l'atelier était-elle suffisamment riche ? 
- Les thématiques des 5 ateliers étaient-elles pertinentes ? *	
- Les échanges en groupe ont-ils permis d'enrichir chacune des thématiques ? *	
- Cette session a-t-elle permis de répondre à vos interrogations ? *	
- Participeriez-vous aux prochains ateliers ? *	


#### --- Le format ---
					
Echelle employée : Très insatisfaisant / Insatisfaisant / Satisfaisant / Très satisfaisant / Sans opinion

- Quel est votre degré de satisfaction concernant : *
    - Qualité de l'image *	
    - Qualité sonore *	
    - Facilité d'utilisation de l'interface *	
    - Documents envoyés en amont de l'atelier (tutoriel de connexion, ressources) *	
    - Animation de l'atelier dans son ensemble *	
    - Animation des séances de travail *	
    - Outil de prise de note mis à disposition *	

- Avez vous des suggestions, remarques à nous faire à ce sujet ?

- L'horaire et la durée de l'atelier étaient-ils adaptés ? *
    - Oui
    - Non
* Si « non », merci de préciser pourquoi : **

- La durée des séances en groupe était-elle adaptée ? *
    - Trop court
    - Bien
    - Trop long

- Souhaitez-vous nous partager un dernier commentaire ?
*(thématique qui vous intéresserait de voir abordée, vos ressentis sur la participation globale à l'atelier, ce qui vous a plu ou que vous souhaiteriez voir amélioré, etc.)*
