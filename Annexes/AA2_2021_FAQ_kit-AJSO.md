---
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---


# Annexe AA1


## A Propos
- Outil suggéré : Page web ou HedgeDoc
- Public cible : internautes (publique)
- Objectif : Exemple de document de type « Foire aux questions » mis à disposition des participants sur toute la période de tenue des ateliers


Accès rapide :

[TOC]


## Contenu du document

###  Titre du document

F.A.Q. (Foire Aux Questions)

AJSO #3 « Données personnelles et ouverture »



### Quels sont les objectifs des ateliers juridiques science ouverte (AJSO) ? 

Dans le contexte des pratiques de recherche en évolution avec la science ouverte, les AJSO sont proposés afin de faire ressortir collectivement des questions légales pouvant se poser tout au long de la recherche. Chaque atelier sera dédié à une thématique transversale ou bien concernera un champ disciplinaire donné. 

Les objectifs visés sont : 
- de définir des questions juridiques clefs issues des retours d'expérience en recherche dans une discipline donnée ou bien pour une thématique transverse ;
- de réunir différents profils en recherche (enseignants-chercheurs, ingénieurs, techniciens, bibliothécaires) avec des juristes afin de trouver des réponses adaptées et facilement compréhensibles par un public de "non-spécialistes" ;
- de mener un travail collaboratif lors de l'atelier avec une phase de préparation en amont pour les participant.e.s ; 
- de participer suite à l'atelier à l'élaboration d'une synthèse permettant de la diffuser et d'impliquer un nombre plus important de personnes.

Objectifs pédagogiques : 
- S’initier à une méthode de travail collaborative et intensive en ligne.
- Réfléchir sur les pratiques de recherche et exprimer des questions relatives aux domaines juridiques.
- Pour les professionnel.le.s de la recherche, apporter des réponses adaptables au cas spécifique de chaque contexte de recherche.
- Pour les juristes, formuler des réponses facilement actionnables pour des "non-juristes".


### Quelle est la démarche des ateliers  ? 

- Les ateliers se veulent pleinement contributifs et initiés sur la base de « cas concrets » (retours d'expérience) et d'un croisement de regard entre les participant.e.s mêlant différents profils (recherche, droit, etc.).
- Des groupes seront constitués en amont de la demi-journée pour traiter plus spécifiquement certaines questions lors de l'atelier. 
- Les ateliers nécessitent une participation active des personnes car ils ont vocation de constituer un réseau d'acteurs qui pourront continuer à échanger sur ces questions par la suite et de participer à la diffusion et à l'appropriation de ces contenus.


### Quels outils seront mis à disposition ? 

L'atelier se déroulera à distance via l'outil de visio-conférence BigBlueButton. Il y aura aussi bien des séances plénières que des salles spécifiques mises en place pour le travail en sous-groupe.


### Que deviendront les contenus produits ? 

Les réponses aux questions juridiques seront regroupées sur la plateforme collaborative Sharelex qui proposera sous forme de questions/réponses les différentes problématiques soulevées et d'y inscrire les pistes de réponses abordées lors de l'atelier. Ces contenus seront partagés sous licence Creative Commons CC-BY-SA. 


### Quelle(s) suite(s) après l'atelier ? 

- La plateforme Sharelex offira un espace pour continuer à nourrir les réflexions communes à distance (de manière synchrone ou asynchrone) suite à l'atelier. Les conditions d'avancement suite à ce temps de rencontre et de travail collaboratif seront abordées en fin d'atelier. 

- Les ateliers juridiques science ouverte (AJSO) ont pour objectif de favoriser la création d'un réseau d'acteurs souhaitant continuer à avancer sur cette problématique à plus long terme. Les modalités de partage et d'échange suite à l'atelier - notamment pour consolider le travail effectué lors de la séance - seront discutées en fin de chaque atelier. 


### Quelles sont les modalités de participation à distance ? 

L'atelier aura lieu à distance via Big Blue Button. Pour assurer les meilleures conditions d'échange, un ensemble de consignes vous sera fourni pour la connexion et les réglages de votre micro. Un test sera proposé un jour avant l'évènement pour vérifier que votre installation fonctionne correctement.


### Y a-t-il besoin de préparer l'atelier en amont ? 

Il vous sera demandé en amont de l'évènement de prendre connaissance d'un document regroupant différentes ressources et pistes de réponses à des questions juridiques déjà posées sur la thématique. Il vous sera possible de commenter avec vos propres cas d'étude ou bien de transmettre vos propres questions.

Le lien du labolex sur la plateforme Sharelex vous sera également envoyé si vous souhaitez vous inscrire et vous familiariser avec la plateforme. 

Un dossier mémo participant.e.s vous sera fourni avant l'atelier afin de vous préparer et d'avoir l'ensemble des informations de connexion par visio-conférence. 


### Qui sont les organisateurs de l'atelier ? 

Les ateliers juridiques science ouverte sont organisés dans le cadre de la mission « réussir l'appropriation de la science ouverte » commanditée par l'Université de Paris (pilotage Anne Vanet) pour le Comité pour la Science Ouverte (CoSO) et le collège « Données de la recherche ».  Juliette Hueber (InVisu, CNRS-InHA) et Lionel Maurel (InSHS, chargé du GT "juridique" du CoSO) sont les personnes qui coordonnent ces ateliers. 

Le cabinet de conseil [inno³](https://inno3.fr/) aide au design des ateliers, à leur facilitation et à la valorisation des contenus produits lors de chaque atelier. La solution de visio-conférence BigBlueButton est mise à disposition par [Worteks](https://www.worteks.com), société spécialisée dans l'expertise Open Source. 
