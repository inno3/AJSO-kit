---
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---


# Annexe AC1


## A Propos
- Outil suggéré : Billet de blog
- Public cible : internautes (publique)
- Objectif : Exemple de billet de blog de présentation de l’AJSO #3 intitulée Atelier Juridique Science Ouverte #3 : « Données personnelles et ouverture » le 28 mai de 9h à 12h30


Accès rapide :

[TOC]


## Contenu du document

###  Titre du document
Atelier Juridique Science Ouverte #3 : « Données personnelles et ouverture » le 28 mai de 9h à 12h30 


### Châpo

Le vendredi 28 mai 2021, de 9h00 à 12h30, aura lieu en visioconférence le troisième atelier juridique science ouverte (AJSO#3) intitulé « Données personnelles et ouverture ».

Les ateliers juridiques science ouverte sont organisés dans le cadre de la mission « réussir l’appropriation de la science ouverte » commandité par l’Université de Paris pour le Comité pour la science ouverte (CoSO) et le collège « Données de la recherche ». Juliette Hueber (InVisu, CNRSInHA) et Lionel Maurel (InSHS, chargé du GT “juridique” du CoSO) ont coordonner ces ateliers. 


### Présentation générale de l’atelier

Depuis l'entrée en vigueur du Réglement Général de Protection des Données (RGPD) en 2016, la gestion des données personnelles a pris une place prépondérante dans la constitution et le déroulement de projets de recherche et d'enseignement supérieur. Plus encore, la mise en conformité vis-à-vis du RGPD s'est accompagnée d'une définition de processus rigoureux et d'une restructurations organisationelles impliquant notamment la désignation d'un ou d'une déléguée à la protection des données (DPO) par établissement. 
À ce même moment, l'ouverture de la science (ouverture des publications et des données) devenait une direction institutionnelle forte à l'échelle européenne et dans chacun de ces État-Membres. 

> La phrase "open as possible, closed as necessary" - souvent employée aujourd'hui - résume bien ce jeu d'équilibriste entre obligation de protection d'une part - entre autre pour des raisons de données personnelles - et incitation à l'échange et au partage des connaissances d'autre part. Le droit apparaît comme un élément clef pour l'articulation de ces deux notions aux abords antinomiques. 

Plus qu'une problématique de mise en conformité, les questions juridiques peuvent être un levier d'ouverture, et source d'une meilleure compréhension des enjeux de science ouverte, et de son intrication avec d'autres dimensions éthiques, techniques, organisationnelles ou encore économiques au sein de l'Enseignement Supérieur et de la Recherche. 


Quelques exemples de questions :  
- J'ai un corpus contenant des données personnelles. Comment ouvrir ces données avec les personnes avec qui je collabore ? 
- Comment recueillir un consentement permettant l'ouverture des données des personnes impliquées dans la recherche ? 
- Existe-t-il des exceptions juridiques donnant lieu à une ouverture possible de données personnelles ? 
- Qui est responsable de la procédure d'anonymisation au sein de mon établissement ? Quels critères à respecter ? 
- Quels critères s'appliquent pour des données personnelles publiquement accessibles sur les réseaux sociaux ? Puis-je les utiliser ? Dans quelles conditions ? 


Par le biais d'un format contributif, l'atelier vise à recenser et à formuler les questions principales à la croisée entre "données personnelles et science ouverte" en se nourissant de différents cas d'études et retours d'expérience des participant.e.s. À la fin de la séance, l'objectif sera d'apporter des premiers éléments de réponses collectives à ces interrogations et d'exprimer différentes pistes d'actions possibles pour faciliter l'appropriation de ces thématiques par une diversité de professionnels de profils et statuts différents présents au sein de l'ESR. 

Au vu des conditions sanitaires, cet atelier se déroulera par visioconférence. Les informations de connexion seront fournies suite à l’inscription. Le nombre de places est limité, remplissez ce questionnaire (insérer le lien vers formulaire d'inscription) pour vous inscrire.

Lors   de   la   séance,   les   personnes   présentes   seront   réparties   en   sous-groupe   afin   d’avancer collectivement sur les différentes thématiques proposées ci-dessous : 

- 1. Anonymisation 
- 2. Consentement 
- 3. Exceptions légales et réglementaires 
- 4. Données personnelles publiquement accessibles (réseaux sociaux numériques)
- 5. Partage sécurisé  



### Informations pratiques et inscriptions

L’atelier juridique « Données personnelles et ouvertes » aura lieu le 28 mai de 9h00 à 12h30. Au vu des conditions sanitaires, cet atelier se déroulera par visioconférence. Les informations de connexion seront fournies suite à l’inscription. Le nombre de places est limité, remplissez ce questionnaire pour vous inscrire.



### Format collaboratif et public convié 

L’atelier juridique « Données personnelles et ouverture » propose de convier :
- différents profils de professionnel.le.s de la recherche (enseignant-chercheurs, chercheurs, conservateurs, personnels de soutien à la recherche, métiers de la documentation et des bibliothèques) confrontés à cette problématique ; 
- des juristes souhaitant aider à répondre à ces questions et sensibiliser à une démarche de co-production pour trouver conjointement des réponses adaptées et compréhensibles pour un public « non-spécialiste ». 

L’atelier n’est pas une formation juridique (ni un bureau juridique) et demande une participation active des participant.e.s. Lors de la séance, chacun.e pourra faire part de ses retours d’expérience et connaissances (juridiques, de terrain, etc.). Il sera aussi proposé de participer, à la suite de l’atelier, à l’élaboration d’une synthèse permettant de la diffuser et d’impliquer un nombre plus important de personnes.

Les ateliers juridiques science ouverte (AJSO) ont aussi pour objectif de favoriser la création d’un réseau de personnes souhaitant continuer à avancer sur cette problématique à plus long terme.


Pour toutes informations supplémentaires, une [FAQ](./2021_FAQ_kit-AJSO.md) est mise à disposition.

Découvrez les autres ateliers juridiques science ouverte 
- AJSO#1 “Au fil des images de la recherche" 
- AJSO#2 "Cycle de vie des données : cartographie collective des enjeux juridiques"


-----

