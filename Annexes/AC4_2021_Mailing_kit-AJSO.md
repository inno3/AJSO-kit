---
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---


# Annexe AC4


## A Propos
- Outil suggéré : HedgeDoc
- Public cible : participant.e.s (public)
- Objectif : Exemples de 3 courriels envoyés aux participant.e.s faisant référence à l’AJSO #3

> La solution choisie pour l'envoi de mail est le logiciel open source [MailTrain](https://mailtrain.org/). Vous trouverez ci-dessous le texte brut sans la mise en page. 


Accès rapide :

[TOC]



## Mail envoyé suite à l'inscription

Objet du mail : [AJSO #3] Atelier 28/05/21 "Données personnelles et ouverture" : informations pratiques

---

ATELIER JURIDIQUE SCIENCE OUVERTE #3

![image de présentation de l'atelier](./pictures/2021_Images-presentation_kit-AJSO.png)

Vous vous êtes inscrit.e.s à l'atelier juridique science ouverte #3 « Données personnelles et ouverture » du 28 mai 2021 de 9h à 12h30.


**Préparation de l'atelier**

En amont de l'atelier, nous vous recommandons de lire attentivement la documentation mise à disposition en cliquant sur les liens ci-dessous :
- [Informations utiles] : ce document résume les informations dont vous aurez besoin pour participer à l'atelier.
- [Préparation de l'atelier] : ce document fournit quelques ressources juridiques, les questions que vous avez formulées lors de votre inscription. Nous y indiquerons aussi la répaertition des participants aux ateliers thématiques à partir du 26 mai lorsque les inscriptions seront closes.

Un test est proposé le jeudi 27 mai à 14h30 (durée max 15 min) afin de vous familiariser avec l'outil de visio-conférence et de vous accompagner dans les éventuels réglages (voir lien ci-après). Cette session vous permettra de vérifier votre accès à la salle ainsi que le bon fonctionnement de votre micro. Il nous sera plus difficile de prendre en charge vos problématiques techniques (micro, caméra, etc.) le jour J. Consultez le tutoriel de connexion pour plus d'informations.

> :warning: Pour tout désistement, merci de nous prévenir au plus vite afin de garantir le bon déroulement du travail en équipe. Nous vous demandons de ne pas inviter des personnes non inscrites car les groupes sont constitués à l'avance. Une synthèse sera mise à disposition par la suite pour les personnes n'ayant pas pu s'inscrire.



**Rejoindre le test et l'atelier**

Ces deux moments utiliserons le même lien de connexion
Test : Jeudi 27 mai, 14h30 (durée 15 min)
Atelier : Vendredi 28 mai, 9h (accessible dès 8h50)


1. Cliquez sur le lien ci-dessous puis rejoignez la session à l'heure et à la date indiquées :

> Rejoindre le test et l'atelier (Bouton avec le lien)


2. Entrez le code d'accès suivant : XXX. Puis indiquez votre nom.

3. Vous êtes connecté.e !

- L'atelier démarrera à 9h, vous pourrez y accéder dès 8h50.
- Votre micro et votre caméra seront automatiquement coupés lors de votre connexion, vous pourrez les activer manuellement.

> :bulb: *Si vous avez des questions et commentaires, merci de nous écrire à : atelier-juridique-so@inno3.fr*




## Mail de relance (une semaine avant l'atelier) 

Objet : [AJSO #3] Atelier « Données personnelles et ouverture » : ce vendredi 28/05 (9h-12h30) 

à adapter sur le modèle du précédent




## Mail de retour (3 jours maximum après l'atelier)

Objet : [AJSO #3] Suite à l'atelier « Données personnelles et ouverture » : votre retour

ATELIER JURIDIQUE SCIENCE OUVERTE #3

![image de présentation de l'atelier](./pictures/2021_Images-presentation_kit-AJSO.png)

Merci de votre participation à l'atelier juridique science ouverte #3 qui s'est déroulé le vendredi 28 mai 2021.

**Votre retour nous est précieux**

Nous vous remercions de bien vouloir consacrer 2 minutes à ce questionnaire de satisfaction afin de répondre au mieux à vos attentes et d'évaluer votre expérience.

> Répondre au questionnaire de satisfaction (Bouton cliquable)


**La suite des échanges**

Nous vous tiendrons au courant via cette liste de diffusion de la mise en ligne de la synthèse de l'atelier. D'ici là, les prises de notes restent accessibles.
