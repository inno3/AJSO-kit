---
robots: index, follow
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---

# Annexe AG2

Rétroplanning indicatif avec toutes les tâches à réaliser vous permettant de provisionner un outil de gestion des tâches comme Kanboard

## A Propos
- Outil suggéré : [Kanboard](https://kanboard.org/)
- Public cible : équipe de facilitation
- Objectif : Gestion de projets ((retro)planning, liste de tâches et répartition)


![exemple tâche kanboard](.pictures/2021_exemple-kanboard_kit-AJSO.png)



## Contenu du document

### Retroplanning indicatif
   
- J-21 :
    - réunion de lancement (temps 0)
    - finalisation présentation ateliers 
    - envoi mail aux facilitateurs potentiels 
    - préparation formulaire d'inscription sur ++framaform++
    - mise à jour de la salle ++BigBlueButton++
    - préparation mail d'annonce événement sur mailtrain

- J-14: 
    - annonce évènement via ++mailtrain++ et communication réseau
    - réunion d'1 heure avec les facilitateurs choisis (temps 1)
    - création ou mise à jour des documents ressources ++hedgedoc++
    - préparation mail de connexion après inscription sur ++mailtrain++

- J-7 
    - réunion (temps 2) ou rappel consigne facilitation à l'équipe concernée 
    - préparation de la préparation générale du déroulé de l'atelier sur ++hedgedoc++
    - arrêt des inscriptions et répartition en sous-groupes 
    - MAJ document repartition groupe pour les participants
    - session test J-1 

- JOUR J 9h -12:30
    - AJSO Jour J sur ++BigBlueButton++

- 2021-06-04 à 14h 
    - retour bilan atelier avec équipe facilitation 
    - rédation enquête de satisfaction sur ++framaform++
    - envoi mail remerciement, enquête et suite via ++mailtrain++
    - retravail des prises de notes par chaque facilitateur.rice

- 2021-06-07
    - rédaction d'une synthèse ou mindmap sur ++open office++ ou ++framindmap++
    - partage de la synthèse aux participants via ++mailtrain++

