---
robots: index, follow
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---

# Annexe AG1
Exemple de document d’organisation interne utilisé pour l’atelier AJSO #3 « Données personnelles et ouverture »

## A Propos
- Outil suggéré : HedgeDoc
- Public cible : équipe de facilitation
- Objectif : Ce document est partagé avec l'équipe de facilitation de l'atelier. Cela leur permet d'avoir accès à l'ensemble des liens de l'atelier ainsi qu'à la trame de facilitation et aux conseils associés. Au sein de ce document, les notes de réunion (avant, après l'évènement) sont aussi prises. À la fin du document, un retroplanning est indiqué avec l'ensemble des actions à mener par la personne en charge de la coordination. 

Accès rapide :

[TOC]


## Contenu du document


### (Titre du document)

Documentation d'organisation interne de l’atelier AJSO #3 « Données personnelles et ouverture »

Date : du XX/XX/XXXX de XXHXX à XXHXX

Lieu : (en ligne ou présentiel avec lien ou adresse)



### Accès rapide vers les ressources et la documentation 

Organisation de l'équipe
- [Informations utiles](./2021_Coordination-interne_kit-AJSO.md) 
- [Préparation de l'atelier avec la liste des questions et les ressources utiles](./2021_Preparation-atelier_kit-AJSO.md)
- [Slides session plénière](./2021_Presentation_kit-AJSO.md) 
- Notes session plénière (_indiquer le lien vers votre document_)

Diffusé auprès des partitipant.e.s
- [Tutoriel connexion et pratiques collaboratives](./2021_Tutoriel-connexion_kit-AJSO.md)
- [Présentation de l'atelier sur ouvrirlascience.fr](https://www.ouvrirlascience.fr/atelier-juridique-science-ouverte-donnees-personnelles-et-ouverture/)
- [FAQ](./2021_FAQ_kit-AJSO.md)

Documentation des groupes de travail
- Groupe 1 : Anonymisation (_indiquer le lien vers votre document_)
- Groupe 2 : Consentement (_indiquer le lien vers votre document_)
- Groupe 3 : Exceptions légales et réglementaires (_indiquer le lien vers votre document_)
- Groupe 4 : Données publiquement accessibles (_indiquer le lien vers votre document_)
- Groupe 5 : Partage sécurisé (_indiquer le lien vers votre document_)


### Agenda de l'atelier 
:clock9: 9h-9h15 Accueil et introduction générale `Plénière`
- Accueil des participant.e.s 
- Rappel logistique et déroulé général 
- Présentation générale des AJSO 
- Objectifs de l'atelier 

:clock930: 9h15-9h45 Contexte juridique et présentation des ateliers `Plénière`
- Présentation du thème et du programme
- Rappel de quelques notions clefs juridiques 
- Présentation des groupes et des facilitat.eurs.rices
- Consignes pour la répartition en sous-groupes et méthodologie de travail


:clock10: 10h-11h30 Ateliers thématiques `Groupe`
- Travail en équipe avec une répartition des différents profils (juristes, chercheur.e.s, personnels IST, etc.) dans chaque groupe
- Accompagnement par les personnes « facilitatrices » de l'atelier

:clock1130: 11h30-11h45 Pause `Plénière`

:clock12: 11h45-12h30 Synthèses et conclusion `Plénière`
- Synthèse des avancées de chaque groupe 
- Conclusion et suite des échanges


### Animation des groupes de travail

**Répartition des groupes (facilitation)**
1. Anonymisation (*nom de la personne facilitatrice*)
2. Consentement (*nom de la personne facilitatrice*)
3. Exceptions légales et réglementaires (*nom de la personne facilitatrice*)
4. Données personnelles publiquement accessibles (*nom de la personne facilitatrice*)
5. Partage sécurisé (*nom de la personne facilitatrice*)


**Proposition de trame pour les personnes facilitant les groupes**

Durée des ateliers  : environ 1h30. 

> Nous finirons la session plénière vers 9h45 et vous pouvez compter 10-15 minutes de battement avant de commencer, histoire que chacun.e prenne ses marques. Les ateliers en sous-groupe se finiront à 11H30. La restitution finale commencera à 11h45 (possibilité d'une pause et de la finalisation des derniers détails).

> :warning: Attention le temps des phases est indicatif et dépendra de l'heure exacte du début de l'atelier. Adaptez en fonction de l'heure de début en notant que la bascule en plénière sera à 11H30. 


#### 1/ Introduction par la personne qui facilite : détails techniques, déroulé et objectifs (10 minutes) 

1.1. Rappels techniques : 
- mettre le lien vers le `BigBlueButton` du sous-groupe dans la salle plénière pour être sûr.e que les participant.e.s vous rejoignent (par exemple Groupe 1 anonymisation + "le lien" )
- une fois les personnes dans la salle de visioconférence, faire un rappel sur les bonnes pratiques :
    - micro coupé lorqu'on ne parle pas
    - mettre la caméra si on le souhaite (plus convivial)
    - indiquer l'espace "discussion publique" et de "notes partagées"
    - mettre le lien du `HedgeDoc` dans les notes partagées et la discussion publique
    - partager la trame de facilitation si vous le souhaitez dans les notes partagées
    - rappel de la bascule à 11H30 en session plénière

1.2. Rappel des objectifs : 
- Rappeler que le format n'est pas un bureau juridique (réponses de juristes aux questions). 
- Il se peut que toutes les questions ne soient pas abordées dans l'immédiat.
- Le but est que chaque personne du groupe puisse apporter ses compétences afin de définir différentes problématiques clefs, de formuler des éléments de réponses adaptées à différents cas de figures et compréhensibles par différents publics, de faire ressortir des bonnes pratiques et d'indiquer des ressources utiles.
- Rappeler qu'il n'y a pas de questions bêtes et que tout retour d'expérience est intéressant 
- Demander à ce que des conversations en sous-groupes ne se mettent pas en place (écoute et travail commun durant 1h30)
- Présenter le déroulé type proposé (ci-dessus) et le copier dans les notes partagés et voir si tout le monde est d'accord

1.3. Répartition des rôles, demander si :
- une personne est à l'aise avec `HedgeDoc` et veut prendre des notes ou trouver une autre solution ;
- une personne veut bien être la "gardienne du temps" ;
- une personne souhaite regarder la discussion publique (copie-colle les informations utiles dans le hedgedoc ou relaie les informations logistiques au groupe);
- une personne peut faire la synthèse en session plénière.


#### 2/ Tour de table et partage d'expérience : génération d'idées (30 minutes)

2.1. Chacun.e se présente et dit ce que la thématique du groupe lui évoque (question, sous-thématiques, etc.) pour poser à plat toutes les questions qui pourraient être associées à cette thématique, les cas d'études/retours d'expérience déjà rencontrées. 

> :warning: Essayer d'être dans un temps de parole de 3 minutes par personne pour que chacun.e puisse s'exprimer avant de rentrer plus dans le détail des questions juridiques.

2.2. Selon le temps, il est possible de refaire un tour de table à la fin pour vérifier qu'aucune idée supplémentaire n'est apparue pendant les échanges et vérifier que chacun.e ait pu partager ses idées ou ses retours d'expérience. 

2.3. Documentation sur l'outil de prise de notes collectif
- Prise en note continue des idées par la ou les personnes désignées
- Si des participant.e.s ont des ressources à partager, ces dernières peuvent être indiquées dans le document partagé.
- Le groupe peut regarder le document "préparation de l'atelier" pour voir si des questions ont déjà été posées en amont de l'atelier.

:bulb: A la fin de ce temps, une prise des notes de l'ensemble des idées est disponible dans le `Hedgedoc` avec des ressources utiles.


#### 3/ Structuration des idées apportées : thématiques (30 minutes)

3.1. Relecture rapide :
- Le document partagé est parcouru (partage d'écran possible).

3.2. Regroupements :
- Le groupe essaye de voir si des idées se rejoignent et forment des sous-thématiques.

3.3. Renforcement :
- Pour chaque regroupement ou thématique, invité à compléter avec des cas d'étude, retours d'expériences, questions juridiques qui se posent et les réponses apportées (avec l'ajout des ressources nécessaires).

:bulb: A la fin de ce temps, le `Hedgedoc` permet de faire ressortir différentes thématiques/questions posées et tous les liens utiles correspondants. 


#### 4/ Complétion et préparation de la restitution : synthèse (20 minutes)

4.1 Ajouter des ressources, des recommandations, des pistes d'actions dans le document
4.2 Mise en forme finale du contenu
4.3 S'assurer qu'une personne du groupe a été choisie pour présenter
4.4 Indiquer comment revenir à la salle virtuelle de la plénière (cliquer sur les 3 points verticaux en haut à droite, sinon bascule automatique à 11H30)

:bulb: Pour la rédation de la synthèse finale, la personne facilitant peut partager son adresse mail aux participant.e.s volontaire pour contribuer à la rédaction et/ou relecture


#### 5/ Fin du travail en groupe et retour en plénière (15 minutes)

Temps de pause et "peaufinage" en session plénière si les participant.e.s le souhaitent. 





### Quelques conseils pour la facilitation 

**Facilitation (forme) :**
- Faire un tour de table au début des groupes et activer les vidéos.
- Idem pour la plénière : activer les vidéos des intervenant.e.s qui parlent (au départ et lors de la synthèse) 
- Partager le lien du BBB du sous-groupe dans la discussion publique de la session plénière
- Bien rappeler le lien vers le hedgdoc pour la prise de note au début des ateliers en sous-groupe dans la discusison publique (proposer le hedgedoc pour la prise de notes mais trouver une autre solution si personne ne se sent à l'aise)
- Mettre les informations de la trame de l'atelier dans les notes partagées.
- Compter 15 minutes de mise en route des groupes. Prendre ce moment pour présenter le déroulé de l’atelier
- Répartir les rôles dans le sous-groupe :
    - une ou des personnes pour la prise de note
    - une personne "gardienne du temps" en fonction de la trame et de l'heure finale (11h30)
    - une personne qui regarde régulièrement la discussion publique et fait remonter les infos (copie-colle dans hedgedoc ou relaie les infos logistiques)
    - une personne pour la présentation d'une synthèse en session plénière
- Si on dispose de 2 écrans, prévoir le partage d'écran avec les notes sur un écran pour que les participant.e.s voient la prise de note. 
- Rappeler le fonctionnement des sous-groupes et de BigBlueButton (comment quitter la salle en fin de session ou prévenir de la bascule automatique à 11h30 parfois un peu violente en fin de session).

**Facilitation (contenu) :**
- Rappeller la problématique et l'objectif
- Rappeler le périmètre de la sous-thématique et voir si tout le monde est d'accord
- Faire attention à la prise de parole (2 minutes par personne au début) 
    -  Ne pas plonger tout de suite dans les questions juridiques (vérifier que tout le monde s'est bien exprimé avant de rentrer dans les détails)
    -  Faciliter la mise en confiance : rassure les non-juristes, il n'y a pas de questions bêtes à poser, tout retours d'expérience peut nourir les réflexions.
    -  Essayer de casser le caractère descendant de la transmission d’infos (faire attention qu'il n'y ait pas de discussions entre spécialistes)
- Ne pas vouloir traiter trop d'informations dans le temps imparti
- Aider à l'émergence de nouvelles questions
- Proposer de partager des ressources en fin de document 
- Chaque personne facilitatrice peut donner son adresse mail si certains participant.e.s souhaitent l'aider pour la rédaction de la synthèse

**Anticiper le rendu :**
Une réunion de retour est prévu une semaine plus tard pour : 
- recueillir les retours de chacun.e sur le déroulement, les conseils d'amélioration ;
- connaître les retours des participant.e.s  (suite à l'envoi d'un questionnaire de satisfaction et premier mail de remerciement) ;
- s'organiser pour la rédaction de la synthèse et les délais de rendu.
