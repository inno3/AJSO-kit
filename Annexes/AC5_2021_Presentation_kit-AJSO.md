---
type: slide
slideOptions:
  transition: slide
  theme: beige
slideNumber: false
title: Données personnelles et ouverture.
tags: CoSO, AJSO, juridique, données personnelles
---



# [Atelier juridique science ouverte AJSO#3] « Données personnelles et ouverture »

* [time= Vendredi 28 Mai 2021]
* [name= CoSO, Université de Paris, Inno³]

---

![image présentation atelier](./pictures/2021_Images-presentation_kit-AJSO.png)


---

## Utilisation Big Blue Button 

- Micro coupé par défaut (possibilité de réactiver lors des sous-groupes)
- Vidéo coupée (à activer en sous groupe)
- Espace discussion publique et notes partagées (infos sous-groupes, prise de note collaborative)
- Problème de bande passante (paramètres > économies de données)

---

## Déroulé de l'atelier 

- :clock9: 9h-9h15 Accueil et introduction générale `Plénière`
- :clock930: 9h15-9h45 Contexte juridique et présentation des ateliers `Plénière`
- :clock10: 10h-11h30 Ateliers thématiques `Groupe`
- :clock1130: 11h30-11h45 Pause `Plénière`
- :clock12: 11h45-12h30 Synthèses et conclusion `Plénière`

---

## Présentation des AJSO

Présentation des ateliers juridiques science ouverte [AJSO] par :

*Nom et rôle de la personne présentant*

---

## Présentation de la thématique

Sujet de l'atelier : « données personnelles et ouverture »

- Identifier les articulations possibles entre protection des données personnelles et ouverture : fenêtre de conciliation 
- Questionner la phrase "open as possible, close as necessary"

---

## Fondamentaux juridique

Rappel des fondamentaux juridiques par
 
(*Nom et rôle des personnes*)


---

### Thématiques 

Liste des groupes de travail par thématiques :
- Groupe 1 : Anonymisation (*nom du ou de la facilitatrice*)
- Groupe 2 : Consentement (*nom du ou de la facilitatrice*)
- Groupe 3 : Exceptions légales et réglementaires (*nom du ou de la facilitatrice*)
- Groupe 4 : Données personnelles publiquement acceessibles (*nom du ou de la facilitatrice*)
- Groupe 5 : Partage sécurisé (*nom du ou de la facilitatrice*)

---

## Objectifs des ateliers

Le but est que chaque personne du groupe puisse apporter ses compétences afin de : 
- répertorier les questions posées en fonction des thématiques
- fournir des exemples et des cas d'études
- voir à quelles domaines juridiques chaque question concerne
- apporter des éléments de réponses si possible
- répertorier des contenus et des références utiles

:bulb: : 
- Le format n’est pas un bureau juridique (réponses de juristes aux questions).
- L'objectif est de répertorier les questions et non pas de trouver des réponses à toute (importance d'un croisement de regards entre différents profils)
- Tous retours d'expériences sont utiles !


---

## Consignes : répartition en groupe 

:warning: vérifier votre groupe dans "notes partagées"

![](./pictures/2021_tuto-image1.png)

<!-- Affichage de l'écran "rejoindre la salle de groupe" 
    -  "choisissez une réunion privée à rejoindre" **via le menu déroulant** CHOISIR LE BON NUMERO DE VOTRE GROUPE
    -  cliquez sur "rejoindre la réunion" (bouton bleu). 
 
Après avoir cliquer sur **"rejoindre la réunion"**, pensez à choisir l'option **"microphone"** de façon à pouvoir parler. -->

---

## Consignes : retour en salle plénière

:clock1130: La session en sous-groupe se terminera automatiquement à 11h30.

:bulb: Si vous souhaitez rejoindre la salle plénière avant, cliquez en haut à droite sur les "..." puis "Déconnecter" vous serez automatiquement rebasculé.e dans la salle plénière. Pensez à réactiver votre micro après la session test.

![tuto image 2](./pictures/2021_tuto-image2.pngg =300x150)

---

## Consignes : méthodologie collaborative

- Mise à disposition de "hedgedoc" pour une prise de note collective : lien dans le document [préparation à l'atelier] 
- Rappel des liens utiles dans la partie « Notes partagées » de Big Blue Button
- Trame de facilitation proposée aux facilitateurs et facilitatrices

---

## Quelques conseils pour faciliter les échanges  

- Allumer sa vidéo au début de l’atelier
- Faire un tour de table pour vous connaitre
- Rappeler les objectifs communs de l’atelier
- Répartir les rôles pour 
   1. la prise de note
   2. un regard sur la discussion publique 
   3. le respect du temps imparti
   4. la présentation de la synthèse.
- Couper votre micro lorsque vous ne parlez pas
- Partager des liens vers l'onglet « discussion publique »
- Prévoir un temps pour résumer les échanges pour la synthèse en groupe

---

:warning: Pour tous soucis de connexion, posez vos questions sur l'espace "discussion publique" de la session plénière


---

## Synthèses des ateliers

- 5-6 minutes/groupe 

---

## Conclusion 

- Suite : Labolex Sharelex
- Autres réunions ? 
- Vos retours.

