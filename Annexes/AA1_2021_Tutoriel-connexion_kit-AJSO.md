---
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---

# Annexe AA1


## A Propos
- Outil suggéré : HedgeDoc
- Public cible : internautes (publique)
- Objectif : Tutoriel étapes par étapes à destination des participant.e.s leur permettant d’accéder et participer aux ateliers dans les meilleures conditions possible


Accès rapide :

[TOC]


## Contenu du document

###  Titre du document

TUTORIEL DE CONNEXION & PRATIQUES COLLABORATIVES

AJSO #3 « Données personnelles et ouverture »



### Étape 1 : Confirmation d'inscription 

Félicitations vous êtes inscrit.e à l'atelier « Données personnelles et ouverture ».

Vous avez dû recevoir par mail la confirmation de votre inscription, avec le lien vers les documents, ainsi que ce « tutoriel de connexion ». Conservez bien ce mail jusqu'au jour de l'atelier, c'est sur celui-ci que vous trouverez votre lien d’accès ainsi que le résumé des informations nécessaires au bon déroulement de l'atelier le Jour J. 

> La documentation ci-dessous vous présente les bonnes pratiques que nous vous conseillons de suivre pour vous connecter en toute sérénité et vous organiser pour travailler collaborativement lors des ateliers. 


### Étape 2 : Mini session de test de l'outil de visio-conférence (jeudi 27 mai à 14h30)

Afin de faciliter un déroulé fluide de l'atelier et d'anticiper tout problème technique et de connexion nous vous proposons de réaliser un test de l'outil `BigBlueButton` le **jeudi 27 mai de 14h30 à 15h**. 

Cette session de **quinze minutes maximum** a pour but de vérifier tous les réglages et de vous présenter l'outil avant l'atelier afin d'éviter tout blocage éventuel : 
- LIEN VERS LA SESSION DE TEST : à ajouter
- CODE D'ACCÈS : à ajouter

> :bulb: `BigBlueButton` (BBB) est l'outil open source sur lequel se dérouleront les différentes étapes de l'atelier. Si vous n’avez jamais utilisé BBB nous vous recommandons vivement de participer à cette session pour être certain.e que votre équipement fonctionne correctement le jour de l'atelier.

> :warning: **Notez que nous ne pourrons pas assurer les réglages de chacun.e pendant l’atelier et cela pourra rendre plus difficile votre participation.**


### Étape 3 : Accéder à la séance plénière (28 mai de 9h00 à 12h30)

Le lien de connexion reçu dans votre mail de confirmation vous donnera accès à la plénière qui commencera à 9h00 le 28 mai. Vous pourrez y accéder 10 minutes avant, soit dès 8h50.

> :bulb: Vous pouvez participer à la vision-conférence depuis un ordinateur, une tablette ou un smartphone, connecté à internet.


**a. Vérifier son navigateur**
Avant de suivre le lien renseigné, et si vous rencontrez des difficultés de connexion, nous vous recommandons d'utiliser l'un des 2 navigateurs suivants (sous **leur dernière version**): 
* Firefox
* Chrome / Chromium

**b. Rejoindre le webinaire**
Cliquez ensuite sur le bouton "**Rejoindre l'atelier**" dans votre mail d'inscription pour vous rendre sur l'outil de visio-conférence. 

*Si vous ne retrouvez plus votre mail, voici les informations de connexion : lien XXXX et code d'accès XXXX.*

Un nouvel onglet apparaît alors indiquant le nom de l'atelier auquel vous souhaitez accéder. **Renseignez d'abord le code d'accès** que vous avez reçu dans votre mail de confirmation puis votre nom qui s'indiquera en tant que participant.e.

> :hourglass: Si un curseur tournant apparaît ainsi que la mention *The meeting hasn't started yet* alors vous êtes en avance, l'atelier n'a pas encore commencé.

Si la session a bien commencé, merci de patienter quelques secondes, l'organisatrice va accepter votre connexion. 

Vous avez un peu de retard ? Vous pouvez vous connecter en cours de séance.

> :bulb L'outil `BigBlueButton` supporte aussi les versions mobiles. Vous n'avez pas besoin de télécharger une application, il vous suffit de vous connecter en cliquant sur le lien de connexion dans votre mail.



### Étape 4 : Configuration de votre micro et réglage de la qualité vidéo

Une fois connecté.e, il vous sera possible de configurer votre micro et de faire un test d'écho.

**a. Configuration de l'audio**
Une nouvelle fenêtre apparaît, il vous est maintenant demandé de configurer votre audio. Vous avez le choix entre : 
* Microphone (*Nous vous recommandons cette 1ère option qui vous permettra de prendre la parole lors des ateliers de groupe*)
* Écoute seule 

**b. Paramètres de la qualité vidéo**
Si votre bande passante est faible, vous pouvez, en cliquant sur "options" (en haut à droite) puis sur paramètres, accédez aux critères de gestion des données et désactiver les caméras des autres participant.e.s


### Étape 5 : Participation en plénière 

Lors de la séance plénière, votre micro et votre caméra seront coupés par défaut lors des temps de présentation en salle plénière. Vous pouvez poser vos questions aux intervenant.e.s par le biais de l'onglet « discussion publique» dont l'icône apparaît en haut à droite de votre écran (sur ordinateur). Vous pouvez poser vos questions à n'importe quel moment et ces dernières seront traitées au fur et à mesure.

> :warning: **Si vous rencontrez des difficultés de connexion** : Contactez les organisateurs présents (soit dans la salle plénière, soit dans la salle de réunion lors des ateliers (cf.étape 7) ). Il y aura toujours au moins 1 personne de l'organisation disponible dans la salle plénière tout au long de l'atelier.



### Étape 6 : Participation aux ateliers en groupe thématique : pratiques collaboratives 

De 10h à 11h30, vous serez répartis en groupes constitués de différents profils (juristes, chercheurs, personnels IST, etc.). Chaque groupe sera accompagné d'un.e facilitateur.trice.


**a. Connexion aux salles en groupes**

![Tuto image sous groupes](./pictures/2021_tuto-image1.png)


Pour les temps d'ateliers en groupe, vous n'aurez pas besoin de lien supplémentaire. Le moment venu, nous vous demanderons simplement de cliquer sur l'option "rejoindre la salle de groupe" puis "choisissez une réunion privée à rejoindre" via le menu déroulant puis cliquez sur "rejoindre la réunion" (bouton bleu). Cela vous donnera accès à des salles privées.
 
> :bulb: Après avoir cliquer sur **"rejoindre la réunion"**, pensez à choisir l'option **"microphone"** de façon à pouvoir parler.


**b. Retour en salle plénière** 

![Tuto image sous groupe](./pictures/2021_tuto-image2.png)


Pour le retour en salle plénière, il vous faut vous déconnecter de la réunion privée en cliquant sur les trois points en haut à droite > Déconnexion.

Lorsque le temps de la réunion en petit groupe sera terminé, une bascule automatique se fera vers la salle plénière. La coupure peut être un peu brutale si vous n'êtes pas préparé.e. Vous pouvez regarder le temps restant qui s'affiche en haut de l'écran ou encore demander aux facilitateurs le temps restant. Il vous est également toujours possible d'envoyer des messages dans la salle plénière aux organisatrices et aux organisateurs.  

> :warning: Les groupes sont conçus en amont sur la base de vos réponses au questionnaire d'inscription. Pour un souci de fluidité et de répartition homogène des groupes, il sera difficile de changer de groupe (raison logistique et technique). Néanmoins, si cela est nécessaire, veuillez prévenir votre facilitateur et/ou envoyer un message sur le canal « discussion publique » de la séance plénière. Il vous faudra alors vous déconnecter de la réunion privée (trois points en haut à droite > Déconnexion). Une fois la salle plénière rejointe, la personne responsable de la répartition des groupes vous ajoutera dans le nouveau groupe.


**c. Quelques conseils pour faciliter les échanges**

- mettre sa vidéo au début de l’atelier
- faire un tour de table pour vous connaitre
- rappeler les objectifs communs de l’atelier
- répartir les rôles pour la prise de note, le gardien du temps, et les remontées d'informations de la discucssion publique
- couper votre micro lorsque vous ne parlez pas
- partager des liens vers l'onglet « discussion publique »
- prévoir un temps pour résumer les échanges pour la synthèse en groupe
- faire une pause avant la restitution finale en plénière
- prévoir une personne qui présentera la synthèse


> :bulb: Afin de rédiger collaborativement nous mettrons à votre disposition une instance `HedgeDoc` (*outil de traitement de texte collaboratif*). Si vous ne savez pas comment utiliser cet outil, en voici une rapide description : https://demo.hedgedoc.org/features



### Étape 7 : Retour en salle plénière

Pour rejoindre la salle plénière à la fin du temps de travail en groupe, il vous suffit de cliquer en haut à gauche sur l'onglet « trois points » et cliquer sur déconnexion. Vous serez automatiquement redirigé.e à la salle plénière. À ce moment, cliquez sur le bouton en bas central « rejoindre l'audio » et faire le test echo si nécessaire. Si vous rencontrez des difficultés pensez à rafraichir la page. 

Il se peut que vous soyez également automatiquement basculé.e vers la salle plénière, (si le temps prévu pour le travail en groupe est dépassé). Dans ce cas, vous pourrez reprendre vos échanges en cours dans la salle plénière.



### Étape 8 : Synthèses des groupes 

Une personne de chaque groupe pourra prendre la parole pour faire une synthèse des échanges. Il suffira qu'il active son micro.

Les séances plénières sont enregistrées afin de nous permettre de réécouter les échanges et de préparer en interne la suite des ateliers. Les séances en groupes ne seront quant à elles pas enregistrées. 

Par la suite, nous vous enverrons un mail, à l'adresse renseignée lors de votre inscription, pour vous informer des suites des ateliers. 



### Récap : "Si je rencontre des problèmes de connexion"
 
* Participez au test jeudi 27 mai à 14h30
* Préférez l'utilisation de Firefox ou Chrome/Chromium (dernière version)
* Vous pouvez utiliser votre mobile si vous rencontrez des difficultés depuis votre ordinateur. Il n'y a **pas** d'installation d'application requise (ni sur ordinateur, ni sur mobile)
* Vérifiez bien vos paramètres audio sur votre ordinateur
* Préférez une connexion en filaire ou une bonne connexion WiFi
* Si vous rencontrez des difficultés avec votre microphone, vous pouvez tout de même avec l'option "écoute seule" intéragir via l'espace "discussion publique"

**Bon atelier !**


| N'hésitez pas à contacter l'équipe organisatrice de l'atelier pour toute question en vous adressant à : atelier-juridique-so@inno3.fr | 
| -------- |

