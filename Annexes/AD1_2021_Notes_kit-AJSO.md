---
robots: index, follow
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---

# Annexe AD1


## A Propos
- Outil suggéré : HedgeDoc
- Public cible : participant.e.s de l'atelier
- Objectif : Modèle de document pour la prise de notes d’un atelier en groupe, contenant également des raccourcis vers les principales ressources de l’atelier et quelques bonnes pratiques pour le bon déroulement de l’atelier


Accès rapide :

[TOC]


## Contenu du document


####  Titre du document

Atelier juridique science ouverte AJSO #3 "Données personnelles et ouverture"

Groupe 4 : DONNÉES PERSONNELLES PUBLIQUEMENT ACCESSIBLES



### Rappel des liens vers la documentation 

- Informations utiles() (_lien à ajouter_)
- Notes session plénière (_lien à ajouter_)
- F.A.Q.  (_lien à ajouter_)
- [Présentation de l'atelier général sur ouvrirlascience.fr](https://www.ouvrirlascience.fr/atelier-juridique-science-ouverte-donnees-personnelles-et-ouverture/)




### Facilitation

- Pensez à choisir : 
    - une ou des personnes qui prennent des notes
    - une "gardienne du temps" (fin de l'atelier à 11H30)
    - une personne qui regarde la discussion publique (copie-colle les commentaires dans le hedgdoc ou relaie les informations logistiques au groupe)
    - une personne qui fera la synthèse en plénière

- Si vous n'êtes pas familier.e avec markdown consultez le :question: en haut à droite.

- Pour tout souci de connexion, posez vos questions dans le canal "discussion publique" de la salle plénière. 

Bon atelier ! 


## Présent.e.s 


## Prise de note tour de table


## Synthèse 


## Ressources 
