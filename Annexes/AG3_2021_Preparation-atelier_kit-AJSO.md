---
robots: index, follow
lang: fr
tags: CoSO, AJSO, juridique, données personnelles
---

# Annexe AG1


## A Propos
- Outil suggéré : HedgeDoc
- Public cible : équipe de facilitation
- Objectif : Exemple de document d’organisation interne utilisé pour la préparation de l’atelier AJSO #3 "Données personnelles et ouverture", il contient :
   - Liens vers les documents-ressources internes à l’organisation
   - Liens vers des ressources juridiques externes pour aider à la facilitation des groupes
   - Questions et commentaires des participant.e.s remontés en amont de l’atelier
   - Répartition des participant.e.s dans les groupes de travail thématiques


Accès rapide :

[TOC]


## Contenu du document


### (Titre du document)

Documentation d'organisation interne de l’atelier AJSO #3 « Données personnelles et ouverture »

Date : du XX/XX/XXXX de XXHXX à XXHXX

Lieu : (en ligne ou présentiel avec lien ou adresse)



### Accès rapide vers les ressources et la documentation 

Organisation de l'équipe
- [Informations utiles](./2021_Coordination-interne_kit-AJSO.md) 
- [Préparation de l'atelier avec la liste des questions et les ressources utiles](./2021_Preparation-atelier_kit-AJSO.md)
- [Slides session plénière](./2021_Presentation_kit-AJSO.md) 
- Notes session plénière (_indiquer le lien vers votre document_)

Diffusé auprès des partitipant.e.s
- [Tutoriel connexion et pratiques collaboratives](./2021_Tutoriel-connexion_kit-AJSO.md)
- [Présentation de l'atelier sur ouvrirlascience.fr](https://www.ouvrirlascience.fr/atelier-juridique-science-ouverte-donnees-personnelles-et-ouverture/)
- [FAQ](./2021_FAQ_kit-AJSO.md)

Documentation des groupes de travail
- Groupe 1 : Anonymisation (_indiquer le lien vers votre document_)
- Groupe 2 : Consentement (_indiquer le lien vers votre document_)
- Groupe 3 : Exceptions légales et réglementaires (_indiquer le lien vers votre document_)
- Groupe 4 : Données publiquement accessibles (_indiquer le lien vers votre document_)
- Groupe 5 : Partage sécurisé (_indiquer le lien vers votre document_)




Bonjour, afin de préparer au mieux l'atelier et afin de garantir une collaboration efficace de chaque équipe, nous vous mettons à disposition dans ce document : 
- Des ressources juridiques que vous pouvez consulter avant la tenue de l'atelier.
- La liste des questions juridiques/retours d'expériences soumises lors de l'inscription.
- La répartition des participant.e.s en sous-groupe (mise à jour le 26 mai). 
 

Pour rappel le document [informations utiles](./AG1_2021_Coordination-interne_kit-AJSO.md) vous fournit toutes les indications nécéssaires sur le déroulé et la connexion en visio-conférence. 

## 1. Ressources juridiques utiles à lire avant la rencontre

Une présentation de quelques éléments clefs juridiques sera proposée en session plénière pour faciliter par la suite le travail en groupe.

Pour rappel, cet atelier consistera essentiellement **en un travail en équipe mêlant différents profils et nécessitera une participation active de votre part**. Il n'est pas nécessaire d'avoir des connaissances spécifiques juridiques, tous vos retours d'expérience, présentations de cas d'étude seront utiles. Pour les objectifs et la démarche des ateliers juridiques : vous pouvez consulter la [FAQ](./2021_FAQ_kit-AJSO.md).


### Ressources spécifiques données personnelles et ouverture

- Aurore and Réseau SupDPO, 2019, “[La Vie d’une Donnée Au Regard Des Réglementations ‘CRPA’, ‘RGPD’ et ‘Patrimoine"](https://reseau.supdpo.fr/wp-content/uploads/2019/07/Aurore-SupDPO-Vie-dune-donne%CC%81e-v1.pdf)

- InSHS, 2021, “[Les Sciences Humaines et Sociales et La Protection Des Données Personnel Dans Le Contexte de La Science Ouverte : Guide Pour La Recherche](https://www.inshs.cnrs.fr/sites/institut_inshs/files/pdf/Guide_rgpd_2021.pdf)”.

- Gaullier, Florence, Lorette Dubois, Benjamin Jean, et Laure Kassem, 2019, “[Ouvrir et Réutiliser Les Données à l’ère Du RGPD : Contraintes et Opportunités](https://inno3.fr/sites/default/files/2021-05/2019_06_27_-_Conference_juridique_-_Support.pdf).” Syntec Numérique .

- Le carnet [éthique et droit en SHS]( https://ethiquedroit.hypotheses.org/)


### Ressources proposées par les participant.e.s (sans accès aux documents pour le moment - à compléter lors de l'atelier):

- En lien avec le Data Protection Officer (DPO) de l'Université de Strasbourg, ma composante a mis au point deux ressources :
    - une mention d'information sur le traitement et l'utilisation des données personnelles (FR et EN) à intégrer dans les formulaires d'inscription (en formations diplômantes et non diplômantes), 
    - un formulaire d'autorisation d'exploitation des photos/images filmées (à l'attention de l'intervenant, dans un contexte événementiel)

- Un tableau sur les ressources des différents pays en Europe

### Ressources générales : données et science ouverte


- Présentation de Lionel Maurel au  webinaire organisé par MateSHS  sur la question « [à qui appartiennent les données ?](https://mate-shs.cnrs.fr/actions/tutomate/tuto25-propriete-donnees-lionel-maurel/)» (Septembre 2020).

- MESRI, CoSo, 2020, [Je publie quels sont mes droits ?](https://www.ouvrirlascience.fr/je-publie-quels-sont-mes-droits/)  (2ème édition 2020)

- MESRI, CoSo, 2017, [Ouverture des données de recherche. Guide d’analyse du cadre juridique en France](https://www.ouvrirlascience.fr/wp-content/uploads/2018/11/Guide_Juridique_V2.pdf) (Dec 2017)

- Le [Guide du droit d'auteur : usage et création des ressources numériques](http://www.sup-numerique.gouv.fr/cid94535/guide-du-droit-d-auteur.html), disponible sur le portail numérique de l'enseignement supérieur (dernière mise à jour Octobre 2018)




## 2. Questions posées et retours d'expérience proposés par les participant.e.s en amont de l'atelier

### Questions

*questions issues du questionnaire d'inscription*

												

### Commentaires / retours d'expérience

*commentaires issus du questionnaire d'inscription*


## 3. Répartition des participant.e.s en sous-groupe (MAJ le 26 Mai)

> :bulb: Nous avons fait en sorte que chaque participant.e puisse intégrer un des deux sous-groupes choisis sur le questionnaire d'inscription.



### GROUPE 1 : Anonymisation - facilité par XX

NOM Prénom 
10 pers. environ

### GROUPE 2 : Consentement - facilité par XX

NOM Prénom 
10 pers. environ


### GROUPE 3 : Exceptions légales et réglementaires - facilité par XX

NOM Prénom 
10 pers. environ

### GROUPE 4 : Données personnelles publiquement accessibles (réseaux sociaux numériques) - facilité par XX

NOM Prénom 
10 pers. environ


### GROUPE 5 : Partage sécurisé - facilité par XX

NOM Prénom 
10 pers. environ







