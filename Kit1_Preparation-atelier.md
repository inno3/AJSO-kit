# Étape 1 : préparatifs en amont de l'atelier

Un atelier juridique science ouverte nécessite plusieurs étapes en amont pour que le jour J se déroule de la manière la plus fluide possible pour l'équipe d'organisation tout autant que pour les participant·e·s.

Cette première étape se distingue en 3 phases et vous est présentée sous forme de 3 listes de tâches à réaliser pour préparer votre atelier dans les meilleures conditions :

[TOC]



## CHECK-LIST 1.A — Amorçage

Choix de la thématique
- [ ] Rédiger un texte de présentation de l'atelier et de ses axes d’études (correspondant aux groupes de travail) `Annexe AC1`
- [ ] Définir le public attendu pour l’atelier (participant.e.s) `Annexe AG1`
- [ ] Créer un support graphique d'accompagnement de l'événement (pensez aux formats adaptés aux outils de communication employés tel que courriels, réseaux sociaux ou support imprimés) `Annexe AV1`

Format de l'atelier
- [ ] Fixer une date de tenue de l’atelier `Annexe AG1`
- [ ] Définir le lieu (en ligne / présentiel), réserver un espace et configurer l’environnement (en ligne : paramètres de connexion, code d'accès, etc. / en présentiel : répartition de l’espace, fournitures, eau) `Annexe AG1`
- [ ] Créer un rétroplanning de la rencontre, en pensant le « avant » et le « après » `Annexe AG2`

Liste des acteurs
- [ ] Identifier et contacter les personnes pour la facilitation `Annexe AG1`
- [ ] Lister et contacter des partenaires et sponsors potentiels `Annexe AG1`

Outils collaboratifs
- [ ] Mettre en place d'un espace de travail partagé pour l'équipe d'organisation (cf. outils)
- [ ] Constituer un support de coordination interne sous forme d’un document collaboratif ou utilisation d'un outil tel que Kanboard pour lister l'ensemble des tâches à réaliser `Annexe AG2`
- [ ] Déterminer une adresse de courriel spécifique pour les ateliers


| :clock3: Le temps de préparation, un élément de vigilance à ne pas négliger |
| ------------------------------------- |
| La préparation de l'atelier nécessite la mobilisation d’au moins une personne pour coordonner l'ensemble (chargé·e de projet). Comptez au moins 4 jours de travail pour un atelier tel qu'ils ont été conçus dans le cadre des AJSO. Ce temps pourra être réduit au fil des ateliers et de la constitution des ressources propres à vos ateliers.  |



## CHECK-LIST 1.B — Coordination

Répartition des rôles
- [ ] Coordination : veiller à la bonne mise à jour et répartition des tâches (Kanboard ou autre outil d'organisation) `Annexe AG2`
- [ ] Logistique : veiller à la réservation et bonne configuration d’un espace (salle en présentiel ou outil numérique avec les paramètres de connexion, code d'accès, etc.) `Annexe AG2`
- [ ] Communication : gérer des inscriptions, des listes d'attentes, des réponses aux questions et des éléments de communication associés (courriels, réseaux sociaux, support imprimés) `Annexe AG2`

Co-design et facilitation
- [ ] Itérer avec l’équipe sur le support de préparation de l’atelier `Annexe AG3`
- [ ] Attribuer des rôles pour chaque membre de l'équipe (coordination, logistique, communication, documentation, animation des groupes thématiques,etc.) `Annexe AG1`
- [ ] Préparer des documents-ressources pour les participants (tutoriel de visioconférence, description du déroulé de l'atelier, ressources juridiques, listes des participant·e·s, etc.) `Annexes AC* + AA*`
- [ ] Présenter le déroulé de l'atelier et des conseils de médiation à l'équipe de facilitation `Annexe AC5`
- [ ] Définir les conditions de partage des ressources et licences attribuées `Annexe AG1`

Plan de communication
- [ ] Définir les canaux de communication pour diffusion de l’annonce et des inscriptions
- [ ] Rédiger un formulaire d'inscription `Annexe AC2`
- [ ] Diffuser l'annonce de l'atelier (texte de présentation et lien du formulaire d'inscription) sur les sites et réseaux choisis `Annexe AC1`

| :open_hands: Gouvernance du projet et des données |
| ------------------------------------- |
| Pour chaque atelier ou pour l'ensemble des ateliers, il est préférable de penser d'ores et déjà la manière dont vous allez organiser vos ressources et le rôle de chacun.e.
Créez-vous un partenariat avec un autre institut pour l'organisation de vos ateliers ? Il est important de veiller au respect du RGPD et des données personnelles des participant·e·s. Vous pouvez aussi apposer des licences aux documents que vous produisez et indiquer aux participant·e·s que les contenus seront partagés sous une licence spécifique.  |




## CHECK-LIST 1.C — Pré-test
Synchronisation de l'équipe
- [ ] Planifier une réunion avec l'ensemble de l'équipe d'organisation et de facilitation `Annexe AG2`
- [ ] Fixer le planning précis des interventions le jour J `Annexe AG1`
- [ ] Rappeler le déroulé et les conseils de préparation (répartition des rôles dans chaque groupe thématique) `Annexe AC3`
- [ ] Indiquer le détail sur le nombre d'inscrit·e·s et le profil des participant·e·s

Session de test (recommandé pour les ateliers en ligne)
- [ ] Identifier des participant·e·s volontaires pour une session de test
- [ ] Proposer une date de test, idéalement quelques jours avant la tenue de l’atelier `Annexe AC4`

Uniformisation des ressources
- [ ] Mettre à jour l'ensemble des documents
- [ ] Création de la présentation générale de l'atelier `Annexe AC5`
- [ ] Création des documents de prises de notes partagées pour chaque groupe `Annexe AD1`
- [ ] Répartition des participant·e·s dans des groupes en fonction de leur préférence (formulaire d'inscription) et de leurs profils `Annexe AG3`

Communication
- [ ] Envoyer un courriel de présentation des ateliers et partage des ressources nécessaires (deux semaines avant la tenue de l’atelier) `Annexe AC4`
- [ ] Relancer les participant·e·s à propos de la tenue de l’atelier par courriel via la liste de diffusion (quatre jours avant) `Annexe AC4`
- [ ] Mettre en ligne la répartition des groupes sur les documents mis à disposition

| :checkered_flag: Importance du pré-test |
| ------------------------------------- |
| Une session-test (~30 minutes) avec des participant·e·s volontaires peut être réalisée avant un atelier en ligne pour vérifier les paramètres de connexion et identifier les éventuels points de blocages. Par exemple, en fonction des lieux de connexion, des pare-feux peuvent gêner la connexion à la visioconférence. C’est également une opportunité pour présenter la plateforme aux intéressé·e·s. 
Le jour de l'atelier, ces problématiques sont difficilement traitables et peuvent amener des participant·e·s à se désister.  |
