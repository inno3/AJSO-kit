# Annexes

Liste des ressources disponibles, classées en 6 catégories.
Chaque ressource est identifiée par un code constitué de 3 caractères.

[TOC]

## Gestion
Utilisation pour l’équipe d’organisation et en particulier la personne en charge.

- [Annexe AG1](./Annexes/AG1_2021_Coordination-interne_kit-AJSO.md) `HedgeDoc` : Exemple de document d’organisation interne utilisé pour l’atelier AJSO #3

- [Annexe AG2](./Annexes/AG2_2021_Kanboard_kit-AJSO.md) `KanBoard` : Rétroplanning indicatif avec toutes les tâches à réaliser vous permettant de provisionner un outil de gestion des tâches comme Kanboard

- [Annexe AG3](./AG3_Annexes/2021_Preparation-atelier_kit-AJSO.md) `HedgeDoc` :
   - Exemple de document d’organisation interne utilisé pour l’atelier AJSO #3
   - Liens vers les documents-ressources internes à l’organisation
   - Liens vers des ressources juridiques externes pour aider à la facilitation des groupes
   - Questions et commentaires des participant.e.s remontés en amont de l’atelier
   - Répartition des participant.e.s dans les groupes de travail thématiques


## Documentation
Ressource pour faciliter les moments de prise de notes et synthèse

- [Annexe AD1](./Annexes/AD1_2021_Notes_kit-AJSO.md) `HedgeDoc` Modèle de document pour la prise de notes d’un atelier en groupe, contenant également des raccourcis vers les principales ressources de l’atelier et quelques bonnes pratiques pour le bon déroulement de l’atelier

- [Annexe AD2](https://sharelex.org/c/science-ouverte/65) `Forum de discussion` Plateforme utilisée pour permettre des discussions asynchrones entre les partipant.e.s et l’équipe d’organisation


## Supports de communication
Ressources pour permettre à l’équipe d’organisation de transmettre les informations aux participant.e.s (annonce, formulaire d’inscription, courriels d’invitations et diapositives de présentation)

- [Annexe AC1](./Annexes/AC1_2021_Annonce-event_kit-AJSO.md) `Billet de blog` : Exemple de billet de blog de présentation de l’AJSO #3 intitulée Atelier Juridique Science Ouverte #3 : « Données personnelles et ouverture » le 28 mai de 9h à 12h30

- [Annexe AC2](./Annexes/AC2_2021_Formulaire-inscription_kit-AJSO.md) `Formulaire` :
   - Exemple de formulaire d'inscription (AJSO #3)
   - Exemple d'enquête de satisfaction (AJSO #3)
   - Modèles de formulaire pouvant être dupliqué sur [Framaform](framaforms.org/templates?title_1=science+ouverte&name=inno3)

- [Annexe AC3](./Annexes/AC3_2021_Informations-utiles_kit-AJSO.md) `HedgeDoc` : Exemple de document récapitulant toutes les informations utiles pour les participants (présentation, déroulement, lien de connexion, équipe, accès à la documentation relative à chaque thématique

- [Annexe AC4](./Annexes/AC4_2021_Mailing_kit-AJSO.md) `Couriels` : Exemples de 3 courriels envoyés aux participant.e.s faisant référence à l’AJSO #3 :
   - Mail de confirmation d’inscription de connexion (envoyé suite à l'inscription d’un.e participant.e) contenant également une invitation à une séance de test
   - Mail de relance (une semaine avant l'atelier)
   - Mail de retour (3 jours maximum après l'atelier)

- [Annexe AC5](./Annexes/AC5_2021_Presentation_kit-AJSO.md) `Diapositives` : Diapositives de présentation de l’atelier (déroulement, ateliers thématiques, objectifs, consignes et conseils)



## Aide
Ressources complémentaires à destination des participant.e.s pour anticiper les potentiels points de blocage et questionnements.

- [AA1](./Annexes/AA1_2021_Tutoriel-connexion_kit-AJSO.md) `HedgeDoc` : Tutoriel étapes par étapes à destination des participant.e.s leur permettant d’accéder et participer aux ateliers dans les meilleures conditions possible

- [AA2](./Annexes/AA2_2021_FAQ_kit-AJSO.md) `Page web` : Exemple de document de type « Foire aux questions » mis à disposition des participants sur toute la période de tenue des ateliers



## Visuel
Aperçu de visuel pour appuyer la communication autour des ateliers.

- [AV1](./Annexes/2021_Images-presentation_kit-AJSO.png) `Image` : Support de présentation principal de l’atelier AJSO n°3

- [AV2](./Annexes/2021_contenuavaloriser-bilan-AJSO.png) `Image` : Extrait de la présentation de synthèse des ateliers



## Bilan des ateliers
Travail de synthèse et bilan des ateliers.

- [AB1](./Annexes/mindmap_ajso2.mm)	`Framindmap` : Carte heuristique réalisée dans le cadre de l’ASJO #2. Consultation sur https://framindmap.org/c/maps/1120992/public.

