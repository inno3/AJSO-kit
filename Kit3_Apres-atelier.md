# Étape 3 : synthèse après l'atelier

L'atelier est terminé, avec sa dose de rebondissements possibles. 
Suite à un moment intense d'organisation comme celui-ci, on peut penser que le travail est terminé, mais il est néanmoins nécessaire de valoriser ce qui a été fait et mettre en avant le travail fourni par les personnes présentes.


Dans cette optique, trois étapes jalonnent la suite de l'atelier :

[TOC]


## CHECK-LIST 3.A — Recueil des retours

Il s'agit de connaître l'avis des participant.e.s d'une part et des personnes ayant aidé à la facilitation d'autre part. Cela permet d’améliorer les prochaines sessions en tenant compte de ces retours.

Enquête de satisfaction
- [ ] Préparer l’enquête de satisfaction pour recueillir les commentaires des participant.e.s. Elle permet de repérer si des problèmes ont été rencontrés concernant le format, les outils utilisés, les échanges, et par rapport aux attentes initiales `Annexe AC2`
- [ ] Envoyer l’enquête de satisfaction par courrier maximum 3 jours après l'atelier `Annexe AC4`
- [ ] Analyser les retours. Ces éléments sont une opportunité d'adapter les prochains ateliers en mettant à jour votre propre kit d’organisation d’atelier (et/ou mettre à jour ce kit)

Réunion de bilan
- [ ] Planifier une réunion (une semaine après l'atelier) avec l'équipe de facilitation `Annexe AG2`
- [ ] Prendre en note les points mentionnés pendant cette réunion. Cela offre la possibilité de revenir sur le déroulé de la séance (ce qui a fonctionné ou pas, les éléments à améliorer, etc. ) et de se répartir les tâches supplémentaires à mener pour la rédaction d'une synthèse


## FICHE 3.B — Synthèse
Rédaction de la synthèse et communication

La production de la synthèse des échanges est là encore une étape chronophage. Elle demande une relecture attentive de l'ensemble des notes prises lors des ateliers afin d'en tirer des thématiques transversales à articuler.

Plusieurs modèles ont été testés en fonction des ateliers :
- Une synthèse sous forme de document texte avec la mise en ligne de quelques questions/réponses sur la plateforme Sharelex `Annexe AD2`
- Une carte heuristique reprenant les éléments clefs des sous-groupes `Annexe AB1`

La synthèse est un travail initial qui a vocation à être partagé aux participant.e.s pour une éventuelle amélioration. Il s'agit d'une première étape qui nécessite de prévoir un temps supplémentaire pour consolider l'ensemble des contenus produits sous la forme de visualisation graphique (`Annexe AB2`).

Prévoir éventuellement une conférence de restitution des synthèses auprès du public.

Un autre enjeu concerne la pérennité et la mise à jour des ressources, qui reste encore une problématique en cours de discussion à la suite de ces premiers AJSO.

| :movie_camera: Restitution de l’atelier |
| ------------------------------------- |
| N’hésitez pas à prévoir une restitution visuelle de vos ateliers sous la forme d’une conférence en ligne, permettant ainsi d’y inviter vos participant.e.s, mais aussi un public plus large. Cela permettra à votre audience de prendre connaissance de vos conclusions de manière plus accessible, et de pointer un rapport plus complet pour les plus curieux.  |



## FICHE 3.C — Mise en réseau
Communiquer régulièrement auprès de votre réseau

Suite à une série de rencontres ayant mobilisé de nombreux acteurs, un réseau, potentiellement ouvert à de nouveau atelier ou d’autres formats à imaginer, s’est constitué, s’est constitué.

Vous pouvez dès lors proposer de nouvelles thématiques d’atelier plébiscitées, voir solliciter de nouveaux partenariats dans l’organisation des prochains ateliers.

Quelques pistes d’actions :
- S’inspirer d’initiatives d’innovation collective
- Mettre en œuvre des cadres de collaboration en s’inspirant des initiatives de communs numérique
- Recueillir les besoins de partenaires en terme de contribution communautaire

Pour cela, mobilisez les outils mis en place et tenez régulièrement ce réseau d’acteurs au courant de futurs ateliers ou d'autres actualités.

| :open_hands: Un cadre de partenariat |
| ------------------------------------- |
| Dans le cas de la mise en place de nouveau partenariat, soyez vigilant à la construction d’un cadre de partenariat et du rôle de chaque personne impliquée. Au même titre, il est important de réfléchir au modes de diffusion des contenus (entrepôts et licences employées) et prendre en considération le temps nécessaire à la mise en place d’une telle mise en réseaux.  |