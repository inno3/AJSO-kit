# Étape 2 : Facilitation pendant l'atelier

Le jour de l'atelier est arrivé, voici quelques conseils pour toute l'équipe. 
Le but étant d'aider à un déroulé fluide de l'ensemble des temps successifs de l'atelier (sessions plénières et en groupes). Nous nous basons sur le déroulé général utilisé lors des AJSO qui se tenaient sur une demi-journée (3h30).

Annexes utiles : `AD1` `AG3`

Cette partie du KIT détaille 4 aspects :

[TOC]


## FICHE 2.A — Déroulé de l'atelier 

:clock9: 9h-9h15 Accueil et introduction générale `Plénière`
- Accueil des participant.e.s 
- Rappel logistique et déroulé général 
- Présentation générale des AJSO 
- Objectifs de l'atelier 

:clock930: 9h15-9h45 Contexte juridique et présentation des ateliers `Plénière`
- Présentation du thème et du programme
- Rappel de quelques notions clefs juridiques 
- Présentation des groupes et des facilitat.eurs.rices
- Consignes pour la répartition en sous-groupes et méthodologie de travail


:clock10: 10h-11h30 Ateliers thématiques `Groupe`
- Travail en équipe avec une répartition des différents profils (juristes, chercheur.e.s, personnels IST, etc.) dans chaque groupe
- Accompagnement par les personnes « facilitatrices » de l'atelier

:clock1130: 11h30-11h45 Pause `Plénière`

:clock12: 11h45-12h30 Synthèses et conclusion `Plénière`
- Synthèse des avancées de chaque groupe 
- Conclusion et suite des échanges


| :clock3: Programmation |
| ------------------------------------- |
| Exemple pour un atelier en ligne de 3h30. 
Le format en ligne nécessitait d’être particulièrement vigilant aux transitions, aux pauses pour éviter ce que l'on appelait en 2020/2021 une « zoom fatigue » (du nom de l'application de visioconférence alors massivement employée à ce moment).  |


## CHECK-LIST 2.B — Coordination

Anticipation
- [ ] Prévoyez d'arriver 15 minutes en avance, même si une seule personne est en charge de la coordination des ateliers. Si cela se produisait, sachez qu'il est difficile pour une personne de prendre en charge l'ensemble des rôles nécessaires au bon déroulé de l'atelier 

Répartition des rôles
- [ ] Pour le jour J, il est préférable de répartir les différentes fonctions entre plusieurs personnes :
- [ ] Logistique : gestion des problèmes de connexion, répartition des personnes dans les salles selon leur groupe, etc.
- [ ] Animation  : introduction, répartition du temps de parole (session plénière) et gestion du temps de l'atelier
- [ ] Documentation  : prise de notes des interventions plénières et aide à la reprise des notes des groupes (mise en forme, synthétisation, organisation des idées, etc.)

Phases de transitions
- [ ] Ne sous-estimez pas le temps nécessaire aux transitions. Les personnes mettront une dizaine de minutes à se connecter ou se déplacer, tout autant qu'à rejoindre les groupes. Il est important de penser à ces temps de battement
- [ ] Le format numérique demande une vigilance importante. Des pauses courtes sont importantes durant l'atelier (au début et à la fin du travail en groupe par exemple)

| :newspaper: Ressource |
| ------------------------------------- |
| Article [Retour d'expérience sur l'utilisation de logiciels libres et open source (notamment Big Blue Button) pour animer des webinaires et ateliers](https://inno3.fr/actualite/retour-dexperience-sur-lutilisation-de-logiciels-libres-et-open-source-notamment-big-blue)  |



## FICHE 2.C — Ateliers en groupes

Gestion de la facilitation : 
- Les sessions en groupe (10-12 personnes maximum) sont des moments privilégiés pour échanger et mettre en commun les différentes questions et connaissances des participant.e.s
- La personne facilitatrice n'a pas pour fonction d'apporter des réponses mais d'accompagner le travail collectif au sein du groupe. Il s'agit plutôt d'un guide pour veiller au bon déroulé dans son ensemble avec l'aide des personnes présentes à l'atelier
- Pour cela, une trame de facilitation est mise à disposition de la personne en charge des groupes avec à la fois un déroulé du travail en groupe proposé ainsi que des conseils et points de vigilance pour les échanges

Les rappels utiles :
- En session plénière, quelques rappels sont aussi donné à l'ensemble des personnes présentes à l'atelier, aussi bien d'ordre technique pour faciliter les échanges en ligne tout autant que sur les finalités du travail et de la répartition de rôles en groupes.
- Proposer de faire un « tour de table » pour que chacun puisse se présenter
- Rappeler les objectifs communs de l’atelier
- Répartir les rôles pour 1. la prise de note, 2. un regard sur la discussion publique 3. sur le temps et 4. une personne pour la présentation de la synthèse
- Activer sa vidéo au début de l’atelier (uniquement en ligne)
- Penser à désactiver votre micro lorsque vous ne parlez pas (uniquement en ligne)
- Partager des liens en utilisant l'onglet « discussion publique » (uniquement en ligne)
- Prévoir un temps pour résumer les échanges pour la rédaction d'une synthèse en groupe

| :art: La facilitation graphique |
| ------------------------------------- |
| Pour organiser collectivement les idées de manière visuelle, il est également possible de mettre en place des outils de facilitation graphique (d’un tableau blanc avec post-it et feutres colorés jusqu’à l’intervention d’un animateur spécialisé) que ce soit en présentiel ou en ligne.
Cela peut fluidifier le partage des idées entre les membres d’un groupe et aussi apporter un soutien visuel au travail de synthèse. |



## FICHE 2.D — Prise de notes

Lors des ateliers, l'outil HedgeDoc a été employé pour prendre des notes collectives de manière synchrone. Cet outil permet d'écrire en markdown (langage à balisage léger pour une mise en page sommaire) avec différentes options de rendu (mode écriture, mise en page, etc.).

La prise de note collaborative synchrone est encore une pratique peu usuelle (en 2021). Cette activité néanmoins est une manière de sensibiliser aux pratiques collaboratives et au langage markdown employé (en 2020/2021) par de nombreuses plateformes d'édition et de travail collectif (wiki, forge, outils d'édition, etc.).

Expérimenter l'écriture collaborative est une opportunité de découvrir les avantages du travail collectif (correction des erreurs, ajout de sources, etc.) et la complémentarité des rôles qui se met en place de façon parfois spontanée entre celles et ceux qui y participent (prise de note, mise en forme, ajout de ressources, etc.).

![aperçu HedgDoc](./pictures/aperçu HedgDoc.png)  
_Aperçu de l’outil HedgeDoc en mode ‘double vue’_


| :art: La syntaxe Markdown |
| ------------------------------------- |
| L’outil open source HedgeDoc propose une page d’aide pour se familiariser avec la syntaxe et toutes les fonctionnalités de mise en page du service. Cette page est accessible via le bouton d’aide « ? » situé dans la barre d’outil en haut de page, côté gauche. |

